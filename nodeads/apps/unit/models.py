from django.db import models
from apps.catalog.models import Category

class Unit(models.Model):

    parent_cat = models.ForeignKey(Category, blank=True,null = True, on_delete=models.SET_NULL, related_name='unit')
    name = models.CharField(max_length = 64)
    description = models.CharField(null = True, blank=True, max_length = 512)
    icon = models.ImageField(null=True, blank=True, default=None)
    create_in = models.DateTimeField(auto_now_add=True)
    moderated = models.NullBooleanField(default=None)

    def __str__(self):
            return "%s" % (self.name)