from django.contrib import admin
from .models import Unit

def moderate_unit_true(modeladmin, request, queryset):
    for obj in queryset:
        obj.moderated = True
        obj.save()

moderate_unit_true.short_description = "Set 'Moderate' as TRUE"

def moderate_unit_false(modeladmin, request, queryset):
    for obj in queryset:
        obj.moderated = False
        obj.save()

moderate_unit_false.short_description = "Set 'Moderate' as FALSE"


class UnitAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'parent_cat', 'description', 'moderated']
    actions = [moderate_unit_true, moderate_unit_false]
    ordering = ('moderated',)

admin.site.register(Unit, UnitAdmin)

