from rest_framework import generics, status
from apps.unit.api.serializers  import NewUnitSerializer


class NewUnitView(generics.CreateAPIView):
    serializer_class = NewUnitSerializer

    def post(self, *args, **kwargs):
            res = super().post(*args, **kwargs)
            res.status_code == status.HTTP_201_CREATED
            return res

