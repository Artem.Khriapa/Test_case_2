from rest_framework import serializers
from apps.unit.models import  Unit

class UnitSerializer(serializers.ModelSerializer):

    # temporary plug. need to fix
    def to_representation(self, data):
        if data.moderated == True :
            #res = data
            return super(UnitSerializer, self).to_representation(data)
        else: return  'Unit is not moderated'

    class Meta:
        model = Unit()
        fields = (
            'id',
            'name',
            'description',
            'moderated'
        )


class NewUnitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Unit
        fields = (
            'parent_cat',
            'name',
            'description',
        )


    def create(self, validated_data):

        newunint = Unit.objects.create(
            name=validated_data['name'],
            description = validated_data['description'],
            parent_cat = validated_data['parent_cat']
        )
        newunint.save()
        return  newunint