from django.conf.urls import url
from apps.unit.api.views import NewUnitView

urlpatterns = [
    url(r'^newunit/$', NewUnitView.as_view()),

]
