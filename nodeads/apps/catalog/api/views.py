from rest_framework import generics, status, filters
from django.shortcuts import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from apps.catalog.api.serializers  import CategorySerializer
from apps.catalog.models import Category as Category_model

class CustomPagePagination(PageNumberPagination):
    #class for set pagination parameters
    page_size = 10 #obj in page
    page_size_query_param = 'page_size'
    max_page_size = 10


class CategoryView(generics.RetrieveAPIView):
    serializer_class = CategorySerializer
    queryset = Category_model.objects.all()

    def get_object(self):
        return get_object_or_404(Category_model, name = self.kwargs.get('cat_name'))


class MainCategoryView(generics.ListAPIView):
    serializer_class = CategorySerializer
    pagination_class = CustomPagePagination
    queryset = Category_model.objects.filter(parent = None)

