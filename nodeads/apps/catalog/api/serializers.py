from rest_framework import serializers
from apps.catalog.models import  Category
from apps.unit.api.serializers import UnitSerializer

class CategoryMinSerializer(serializers.ModelSerializer):

    unit = UnitSerializer(many = True,read_only=True)

    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'category',
            'unit',
        )

class CategorySerializer(CategoryMinSerializer):

    count_units = serializers.ReadOnlyField(source='count_unit')
    count_units = serializers.ReadOnlyField(source='count_unit')
    category = CategoryMinSerializer(read_only=True, many = True)


    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'description',
            'count_units',
            'count_subcats',
            'category',
            'unit',
        )

