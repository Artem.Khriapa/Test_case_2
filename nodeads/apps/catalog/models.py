from django.db import models
import mptt
from mptt.models import MPTTModel, TreeForeignKey


class Category(MPTTModel):

    @property
    def count_unit(self):
        res = self.unit.count()
        return res

    @property
    def count_subcats(self):
        res = self.Category.all().count()
        return res

    parent = TreeForeignKey('self', null=True, blank=True, related_name='category')
    name = models.CharField(max_length=64)
    description = models.CharField(null=True, blank=True, max_length=512)
    image = models.ImageField(null=True, blank=True, default=None)

    class Meta():
        db_table = 'category'

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
            return "-- %s" % (self.name)

mptt.register(Category,)

