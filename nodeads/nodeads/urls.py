from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/catalog/', include('apps.catalog.api.urls')),
    url(r'^api/unit/', include('apps.unit.api.urls')),
]
